---
title: Pretty good privacy
include_footer: true
---

Zum Austausch wichtiger Informationen und Dokumente (Angebote, Verträge etc.) möchte ich hier die Möglichkeit bieten, dies in einer gesicherten Art und Weise tun zu können.

[Pretty Good Privacy](https://de.wikipedia.org/wiki/Pretty_Good_Privacy) ist eine Technology zum Unterschreiben und Verschlüsseln von eMails und Dokumenten.

Mein öffentlicher PGP Schlüssel kann hier heruntergeladen werden:

https://keys.openpgp.org/vks/v1/by-fingerprint/9E3599A128768E07F12A66379AC714D8A035A4A4 \
Fingerprint: `9E35 99A1 2876 8E07 F12A 6637 9AC7 14D8 A035 A4A4`

Folgende Identitäten sind mit ihm verknüpft:

 * k.erber@erber-freelance.de
 * k.erber@isium.de
 * klaus.erber@gmail.com

Ich signiere eMails und wichtige Dokumente mit diesem Schlüssel und es ist möglich mir mit diesem Schlüssel verschlüsselte eMails oder Dokumente zu schicken. Teilen Sie mir gerne Ihren öffentlichen Schlüssel mit, ich werde dann sensible Informationen nicht nur signieren, sondern auch verschlüsselt versenden.

Bitte prüfen Sie meinen Schlüssel mit dem hier angezeigten Fingerprint. Sie können ihm dann direkt vertrauen. Alternativ können Sie auch dem Schlüssel von [governikus.de](https://pgp.governikus.de/pgp/) vertrauen. Ich habe dort meinen Schlüssel durch Vorlage meines elektronischen Personalausweises beglaubigen lassen.

Sie können den öffentlichen Schlüssel von Governikus unter folgenden Link herunterladen:

https://keys.openpgp.org/vks/v1/by-fingerprint/864E8B951ECFC04AF2BB233E5E5CCCB4A4BF43D7 \
Fingerprint: `864E 8B95 1ECF C04A F2BB 233E 5E5C CCB4 A4BF 43D7`



