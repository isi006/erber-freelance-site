---
title: Über mich
sidebar: false # or false to display the sidebar
sidebarlogo: fresh-white-alt # From (static/images/logo/)
include_footer: true
---

<div class="has-margin-top-90"></div>

# Klaus Erber • Cloud Architekt


## Persönliche Daten

<hr />
<div style="display: flex; flex-wrap: wrap; justify-content: space-between">
  <div>
    <img class="content-pic" src="/images/ke_small.jpg">
  </div>
  <div>
    <div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="7b4978dc-e6f1-4acd-a342-eb03a6c3ba21" data-share-badge-host="https://www.credly.com">
    </div>
    <script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>
  </div>
</div>
<div class="print-logos">
  <img class="partner-logo" src="/images/logos/clients/aws.svg">
  <img class="partner-logo" src="/images/logos/clients/java_ee_sm.svg">
  <img class="partner-logo" src="/images/logos/clients/devops.svg">
  <img class="partner-logo" src="/images/logos/clients/kubernetes.svg">
  <img class="partner-logo" src="/images/logos/clients/docker.svg">
  <img class="partner-logo" src="/images/logos/clients/azure.svg">
  <img class="partner-logo" src="/images/logos/clients/gcp.svg">
</div>
<hr style="margin-bottom: 50px;" />

<table class="table data-table">
    <tbody>
      <tr>
        <td>Vorname</td>
        <td>Klaus</td>
      </tr>
      <tr>
        <td>Name</td>
        <td>Erber</td>
      </tr>
      <tr>
        <td>Geburtsdatum</td>
        <td>05.01.1967</td>
      </tr>
      <tr>
        <td>Familienstand</td>
        <td>verheiratet, 2 Kinder</td>
      </tr>
      <tr>
        <td>Staatsangehörigkeit</td>
        <td>deutsch</td>
      </tr>
      <tr>
        <td>Straße</td>
        <td>Pappelreihe 24</td>
      </tr>
      <tr>
        <td>Wohnort</td>
        <td>D-21255 Tostedt (bei Hamburg)</td>
      </tr>
      <tr>
        <td>eMail</td>
        <td>k.erber@erber-freelance.de</td>
      </tr>
      <tr>
        <td>Telefon</td>
        <td>+49 172 6248666</td>
      </tr>
    </tbody>
</table>

<a name="education"></a>
<div class="page-break has-margin-top-90"></div>

<h1 class="title-image"><img src="/images/icons/icons8-school-48.png" />Ausbildung</h1>

<table class="table data-table">
    <tbody>
        <tr>
            <td>Schulbildung</td>
            <td>Realschule</td>
        </tr>
        <tr>
            <td>1986</td>
            <td>Abschluß der Berufsausbildung zum Chemiefacharbeiter</td>
        </tr>
        <tr>
            <td>1991</td>
            <td>Abschluß als Industriemeister Fachrichtung Chemie,
                inkl. Ausbildereignungsprüfung</td>
        </tr>
        <tr>
            <td>1994 - 1998</td>
            <td>Studium der Wirtschaftsinformatik bei der <a href="http://www.akad.de/">AKAD</a>,
                Hochschule für Berufstätige Rendsburg und Abschluß als Diplom-Wirtschaftsinformatiker/FH
                (Diplomarbeit Note 1.7, Kolloquium 1.0, Gesamtnote 'gut').
                Original: <a href="/assets/diplom.pdf">[Zeugnis]</a></td>
        </tr>
        <tr>
            <td>2002</td>
            <td>Ausbildung zum certified TopLink Trainer durch die Fa. Oracle
                <a href="/assets/toplink.pdf">[Zertifikat]</a></td>
        </tr>
        <tr>
            <td>2023</td>
            <td>Ausbildung zum Certified Kubernetes Administrator, Zertifizierung bei der Cloud Native Computing Foundation
                <a href="/assets/klaus-erber-cka.pdf">[Zertifikat]</a></td>
        </tr>
    </tbody>
</table>

<a name="employments" ></a>
<div class="has-margin-top-90"></div>

<h1 class="title-image"><img src="/images/icons/icons8-permanent-job-48.png" />Werdegang</h1>

<table class="table data-table">
  <tbody>
    <tr>
      <th>Zeitraum</th>
      <th>Position/Beschreibung</th>
    </tr>
    <tr>
      <td>01.08.1983&nbsp;-<br />  31.12.1998</td>
      <td><b>Auszubildender, Facharbeiter, Schichtleiter, Betriebsleiter, Softwareentwickler (Nebenbeschäftigung)</b><br />
      unbefristetes Arbeitsverhältnis bei der Fa. ESSO AG danach HOLBORN Europa Raffinerie GmbH, Hamburg,
      <a href="/assets/zeugnis_her.pdf">[Zeugnis]</a></td>
    </tr>
    <tr>
      <td>01.01.1999&nbsp;-<br />  30.06.2001</td>
      <td><b>Geschäftsführer, Systemingenieur</b><br />
      unbefristetes Arbeitsverhältnis bei der Fa. maxheim wirtschaftsinformatik / mwi holding gmbh / mwi hamburg gmbh, Hamburg
        <a href="/assets/zeugnis_mwi.pdf">[Zeugnis]</a>
      </td>
    </tr>
    <tr>
      <td>01.07.2001&nbsp;-<br />  31.03.2020</td>
      <td><b>Senior IT-Consultant</b><br />
      unbefristetes Arbeitsverhältnis bei der Gauss Interprise Consulting AG später
        <a href="http://www.evodion.de">evodion Information Technologies GmbH</a>, Hamburg</td>
    </tr>
    <tr>
      <td>01.08.2015&nbsp;-<br />  31.01.2020</td>
      <td><b>Geschäftsführer</b><br />
      Nebenberufliche Geschäftsführung der evodion Mitarbeiter Beteiligungsgesellschaft GbR, Hamburg</td>
    </tr>
    <tr>
      <td>ab 01.01.2020</td>
      <td><b>IT-Freiberufler</b><br />
      Selbstständiger Cloud Architekt.
    </tr>
  </tbody>
</table>

<a name="skills"></a>
<div class="has-margin-top-90"></div>

<h1 class="title-image"><img src="/images/icons/icons8-search-property-48.png" />Erfahrung</h1>

Der sichere, effiziente und verlässliche Betrieb einer Anwendung ist für mich genauso wichtig, wie die eigentliche Softwareentwicklung. Deshalb habe schon immer entsprechende Aufgaben in Projekten übernommen. Die aktuellen Entwicklungen rund um DevOps-Kulturen, Containerisierung, Kubernetes und Cloud Computing sind bei mir gängige Praxis geworden.

Ich bringe Ihre Anwendungen in die Cloud. Sicher, hochverfügbar, skalierbar, kosteneffizient und ohne Vendor-Lockin.

Ich verbinde Dev und Ops, da ich beiden Welten Zuhause bin. Devs können mit mehr Ops-Wissen wesentlich bessere Architekturentscheidungen treffen und somit ihre Anwendungen cloudfähig machen. Ops können mit entsprechenden Dev-Wissen lernen ihre Infrastruktur in Code zu gießen und somit ein neues Level an Automatisierung erreichen. Zusammen können entwickelte Features viel schneller und in höherer Qualität zum Endbenutzer gebracht werden.

Auch wenn es nicht das Ziel ist eine Anwendung in der Cloud zu betreiben, so sind doch die sich z. Zt. etablierenden DevOps-Ansätze auch für den Betrieb on premisses von großem Vorteil. Denn schnelle, flexible, sichere und automatisierte Verfahren für Softwaretests und -auslieferung wirken sich auch hier positiv aus.

## Rollen

Cloud Architekt, DevOps-Engineer, Senior IT-Consultant, Projektleiter, Senior/Lead Developer, Schulungsleiter

## Softskills

Teamfähig, agil, verlässlich, Know-How-Transfer ins Team, sicherheitsbewusst, empathisch, neugierig, viel Weitblick, kreativ, kritikfähig, Feedbackgeber

## Aktuelle Technologien

*Der Übersichtlichkeit halber, habe ich ältere Technologien aus der Liste gestrichen, da sie in heutigen Projekten keine große Relevanz mehr haben. Im Zweifelsfall, [fragen Sie bitte einfach nach](/#section5), ob ich Ihnen zu solchen Themen weiterhelfen kann. Meine [vollständige Projektliste](/projects_full) kann hier auch hilfreich sein.*

### Betriebssysteme

Linux (debian, Ubuntu, RHEL & Clone), MacOS, Windows

### Virtualisierung

KVM, Proxmox, OpenStack, HyperV, Virtual Box

### Cloudprovider

Google Cloud Platform, SC Synergy (Apache Cloudstack), Azure, AWS, Scaleway, Hetzner, IONOS

### CI/CD

GitHub Actions, Azure DevOps, Gitlab CD/CI, Atlassian Bamboo, GoCD, Gitea, Nexus, Artifactory, FluxCD, Jenkins

### Datenbanken

MySQL, MariaDB, PostgreSQL, MS-SQL, Firestore, Oracle, H2, Redis/ValKey

### Netzwerke

Wireguard VPN, OpenVPN, IPSec, OPNSense, pfSense, OpenWRT, Websockets/STOMP, IoT/MQTT, Routing, HTTP/S

### Middleware/Frameworks

Spring Boot, Google Firebase, RabbitMQ, Keycloak, Node.js, NodeRed, Tomcat, nginx, haproxy, JBoss/Wildfly, Apache-Webserver, JEE, EJB, EJB3, Web Services/REST/SOAP

### DevOps/Cloud

Terraform/OpenTofu, Ansible, Docker, OCI, ELK, Loki, Splunk, Cloudinit, Vagrant, Packer, certbot, restic, Podman/Buildah, Minio, devcontainers

### Kubernetes (Certified Kubernetes Administrator)

Kubernetes kubeadm/RKE2/k3s/aks/gke/cloudstack minikube/kind, Rancher, Istio, Linkerd, Longhorn, Rook/Ceph, k3s, podman, Helm/helmfile, Prometheus, Alertmanager, Grafana, letsencrypt/cert-manager, Operator Development in Golang, PostgreSQL Operators: Zalando & Crunchy Data, Kyverno, velero, vcluster (Kubernetes in Kubernetes), Longhorn, Ceph/Rook, Sealed Secrets, cert-manager


<!-- ### UI Frameworks

Vue.js, WebComponents, Microfrontends, PWA, Angular, React, IONIC, Cordova, Vaadin, Apache Wicket, JSF

### Skript-, Programmier- und Auszeichnungssprachen

Kotlin, Python, Go, Java, Dart, C#, HTML, Typescript, Javascript, CSS, Groovy, C, C++, Visual Basic, SQL, Unix Shellprogrammierung, Perl, PHP, JSP, JSON, YAML, TOML, XML/DTD/XMLSchema, XSLT, AJAX

### Web Content Management

Wordpress, gohugo, Jekyll´

### Methoden

Domain Driven Design, Design Thinking, Personas, UX-Maps, Scrum, Kanban, DevOps, OOA, OOD, OOP, Prototyping, Informationsbedarfsanalyse, Entwurf Systemschnittstellen, Datenbankentwurf konzeptionell und physikalisch, Entwurf Klassenmodelle, Anwendungsverteilung, BSI 100-4 IT-Notfallmanagement

-->

### Skript-, Programmier- und Auszeichnungssprachen

Python, Go, Java, Groovy, Shell, Perl, JSON, YAML, TOML, XML/DTD/XMLSchema

## Sonstige

### Branchen

IT-Dienstleister, Bildung, Pharmaindustrie, Versicherungen, Banken, Leasing, Behörden, Verlage, Presseagenturen, Mineralölindustrie, Steuerberatung

### Führungserfahrungen

Geschäftsführung eines IT-Startups, Geschäftsführung einer Mitarbeiter Beteiligungsgesellschaft, mehrjährige Erfahrung in der Leitung von IT-Projekten (Teamgrößen bis zu 12 Mitarbeiter), mehrjährige Führungserfahrung in der mineralölverarbeitenden Industrie (Teamgrößen bis zu 19 Mitarbeiter)

### Schulungsleitung und Beratung

Trainer für Docker & Kubernetes, Coaching und Beratung im IT-Bereich, Durchführung von Schulungen für Softwareentwickler und Anwender

### Sprachkenntnisse

Deutsch, Englisch

## Wichtige Konzepte

Hier eine Auswahl von Konzepten die ich in Projekten umsetze:

**DevOps** ist ein Ansatz, der Entwicklung (Dev) und Betrieb (Ops) eng miteinander verzahnt, um Software schneller und effizienter bereitzustellen. Durch Automatisierung, kontinuierliche Integration und Zusammenarbeit wird eine hohe Verfügbarkeit, Sicherheit und Qualität der Anwendungen gewährleistet.

**Infrastructure as Code (IaC)** bedeutet, dass die gesamte IT-Infrastruktur wie Netzwerke, Server und Datenbanken in Code beschrieben wird. Dadurch können Umgebungen automatisiert, versioniert und reproduzierbar bereitgestellt und verwaltet werden, was Effizienz und Skalierbarkeit erhöht.

**Container** sind leichtgewichtige, isolierte Umgebungen, in denen Anwendungen mit all ihren Abhängigkeiten ausgeführt werden. Sie ermöglichen eine konsistente Ausführung über verschiedene Umgebungen hinweg und erleichtern die Skalierbarkeit und Portabilität von Anwendungen.

**Orchestrierung** bezeichnet die automatisierte Verwaltung und Koordination von Containern, Netzwerken und Anwendungen in komplexen Umgebungen. Sie sorgt dafür, dass Container gestartet, skaliert, überwacht und bei Ausfällen automatisch neu verteilt werden, um einen reibungslosen Betrieb sicherzustellen.

Der **Zero Trust**-Ansatz geht davon aus, dass kein Benutzer oder System automatisch vertraut wird – egal ob innerhalb oder außerhalb des Netzwerks. Jede Anfrage muss authentifiziert und autorisiert werden, was hilft, die Sicherheit von Anwendungen und Daten zu stärken.

**Identity and Access Management (IAM)** regelt, welche Benutzer und Systeme auf welche Ressourcen zugreifen dürfen. Durch strikte Rollen- und Rechteverwaltung wird sichergestellt, dass nur autorisierte Entitäten Zugriff auf sensible Daten und Systeme haben.

**Verschlüsselung** schützt Daten sowohl im Ruhezustand **(at rest)** als auch während der Übertragung **(in transit)**. Dies stellt sicher, dass sensible Informationen selbst dann sicher bleiben, wenn sie abgefangen oder kompromittiert werden.

Eine **Web Application Firewall (WAF)** schützt Webanwendungen, indem sie eingehenden und ausgehenden HTTP-Datenverkehr überwacht und potenziell bösartige Anfragen blockiert. So werden Angriffe wie SQL-Injections oder Cross-Site-Scripting verhindert.

**Secret Management** befasst sich mit der sicheren Speicherung und Verwaltung von sensiblen Informationen wie API-Schlüsseln, Passwörtern und Zertifikaten. Dienste wie Azure Keyvault oder Google Secrets Manager helfen, diese Geheimnisse sicher zu verwalten und Zugriffe zu kontrollieren.

Der **Least Privilege**-Ansatz stellt sicher, dass Benutzer und Systeme nur die minimal erforderlichen Rechte haben, um ihre Aufgaben zu erfüllen. Dies reduziert das Risiko von Sicherheitsvorfällen und ungewolltem Zugriff.

**Continuous Integration (CI)** ist ein Prozess, bei dem Codeänderungen automatisch Builds, Unit Tests und gegebenenfalls Deployments auslösen. Dies sorgt für eine schnelle Erkennung von Fehlern und ermöglicht eine kontinuierliche Auslieferung von hochwertiger Software.

**Continuous Delivery** erweitert Continuous Integration und ermöglicht die automatisierte Bereitstellung von Codeänderungen. Vor dem Übergang in Staging- oder Produktionsumgebungen sind manuelle Freigaben durch Entwickler oder die Qualitätssicherung notwendig.

**Continuous Deployment** geht einen Schritt weiter als Continuous Delivery und automatisiert die vollständige Auslieferung von Codeänderungen bis in die Produktionsumgebung. Jede erfolgreich getestete Änderung wird ohne manuelle Freigaben sofort produktiv geschaltet, was eine schnelle und kontinuierliche Bereitstellung ermöglicht.

**Monitoring** umfasst das kontinuierliche Beobachten und Messen von Systemen anhand vordefinierter Metriken wie CPU-Auslastung, Speicher oder Netzwerkaktivität. Es dient dazu, Probleme frühzeitig zu erkennen und die Verfügbarkeit der Systeme sicherzustellen.”

**Observability** geht über klassisches Monitoring hinaus und bietet einen umfassenderen Einblick in den Zustand eines Systems. Durch die Kombination von Metriken, Logs und Traces ermöglicht es, Probleme proaktiv zu identifizieren und deren Ursachen tiefgehender zu verstehen.

**Backup & Recovery** bezieht sich auf die regelmäßige Sicherung von Daten, um im Falle eines Verlusts oder einer Beschädigung eine Wiederherstellung zu ermöglichen. Ein durchdachter Backup- und Recovery-Plan stellt sicher, dass kritische Daten und Systeme nach Ausfällen oder Cyberangriffen schnell wiederhergestellt werden können.

**Disaster Recovery (DR)** ist ein strategischer Prozess, der sicherstellt, dass IT-Systeme und Daten nach schwerwiegenden Vorfällen wie Naturkatastrophen, Cyberangriffen oder Hardwareausfällen wiederhergestellt werden können. Ein DR-Plan definiert klare Maßnahmen und Wiederherstellungsziele, um den Geschäftsbetrieb schnellstmöglich zu sichern.

**Cloud Native** beschreibt einen Ansatz zur Entwicklung und Ausführung von Anwendungen, die speziell für Cloud-Umgebungen optimiert sind. Durch den Einsatz von Containern, Microservices und dynamischer Orchestrierung bieten Cloud-Native-Anwendungen eine hohe Skalierbarkeit, Flexibilität und Resilienz.

**High Availability (HA)** bezieht sich auf die Fähigkeit eines Systems oder einer Anwendung, trotz Ausfällen oder Problemen nahezu durchgängig verfügbar zu sein. Dies wird durch redundante Architekturen, automatische Failover-Mechanismen und die Vermeidung von Single Points of Failure erreicht.

**Software Defined Networking (SDN)** ist ein Ansatz, bei dem Netzwerkkonfiguration und -steuerung über Software zentralisiert und automatisiert werden. Dies ermöglicht eine flexible und dynamische Verwaltung von Netzwerkressourcen und eine schnelle Anpassung an wechselnde Anforderungen.

**GitOps** ist ein Ansatz zur Verwaltung und Automatisierung von Infrastruktur und Anwendungen, bei dem das Git-Repository als zentrale Quelle für die gesamte Konfiguration dient. Änderungen werden durch Git-Commits versioniert und automatisch in die Zielumgebung übertragen, wodurch der Prozess transparent und nachvollziehbar wird.

**Self Healing** beschreibt die Fähigkeit eines Systems, automatisch auf Fehler zu reagieren und sich selbst zu reparieren, ohne manuelles Eingreifen. Durch Mechanismen wie automatische Neustarts, Replikation und Ausfallüberwachung wird die Verfügbarkeit und Stabilität des Systems gewährleistet.

**Autoscaling** ermöglicht es einem System, automatisch Ressourcen wie Rechenleistung oder Speicherplatz je nach Last anzupassen. Dadurch wird sichergestellt, dass immer genügend Ressourcen zur Verfügung stehen, um die Anforderungen zu erfüllen, während gleichzeitig Kosten durch Überprovisionierung vermieden werden.

**Serverless** ist ein Cloud-Modell, bei dem Anwendungen ausgeführt werden, ohne dass Entwickler sich um die zugrunde liegende Serverinfrastruktur kümmern müssen. Die Cloud-Anbieter verwalten automatisch die Skalierung und Ressourcen, während die Abrechnung nur für die tatsächlich genutzte Rechenzeit erfolgt.

**Service Mesh** ist eine Infrastrukturebene, die die Kommunikation zwischen Microservices in einer verteilten Anwendung steuert. Es bietet Funktionen wie Load Balancing, Service Discovery, Verschlüsselung, Monitoring und Fehlerbehandlung, ohne dass Änderungen an den Anwendungen selbst erforderlich sind.

<!-- ## Legacy

## Betriebssysteme

Linux, KVM, vSphere, HyperV, MacOS, AIX, SunOS, DOS/Windows9x/NT/2000/XP

## Branchen

IT-Dienstleister, Pharmaindustrie, Versicherungen, Banken, Leasing, Behörden, Verlage, Presseagenturen, Mineralölindustrie, Steuerberatung

## Datenbanken

Firestore, mongodb, DB2, Oracle, H2, Informix, MySQL, PostgreSQL, sapdb, ADABAS D, MS-SQL Server, MS-Access, dBase, Derby, HSQL

## Entwicklungsumgebungen

IntelliJ, Visual Studio Code, Visual Studio, XCode, Eclipse, Netbeans, Oracle JDeveloper und TopLink Mapping Workbench, Visual Age for Java, JBuilder, VisualCafe, Borland C, Delphi, JDK, GNU C/C++,
RationalRose/MS-Visual Modeler, Visual Basic und VBA, Visual C++

## Methoden

Domain Driven Design, Design Thinking, Personas, UX-Maps, OOA, OOD, OOP, Prototyping, Informationsbedarfsanalyse, Analyse DV-Technik, Entwurf Systemschnittstellen, Datenbankentwurf konzeptionell und physikalisch, Entwurf Klassenmodelle, Anwendungsverteilung in Softwarekomponenten

## Netzwerke

Wireguard VPN, Websockets/STOMP, IoT/MQTT, Routing, NAT, X-Protokoll, Einrichtung diverser Netzdienste, wie z. B. bootp, http, ftp, smb, mail (SMTP, imap4 u. pop3)</p>

## Middleware

Spring Boot, Micronaut, Quarkus, Node.js, NodeRed, nginx, haproxy, JBoss/Wildfly, Apache-Webserver, JEE, EJB, EJB3, CORBA, CICS, UNIKIX, Web Services/REST/SOAP, Sockets

## Produkte/Frameworks

AXON/Eventsourcing, github, gitlab, JIRA, Confluence, Bitbucket, GIT, SVN, CVS, EclipseLink, Affinity Designer/Photo, Oracle SQL-Developer, Tomcat, YourKit Java Profiler, BugZilla, Mantis, jboss, Java WEB-Start, Ant, XDoclet, ErWin, KDE, Gnome, VMware, Mambo/Joomla/typo3 Content Management, Wiki, Bind, Squid, SSH, Webmin, MS-Office, The Gimp

## DevOps/Cloud

Docker, Kubernetes, jenkins, Google Clout Platform, Azure, AWS, Cloudinit, Ansible, Vagrant, Packer, Terraform, letsencrypt/certbot, restic, consul

## UI Frameworks

Vue.js, Angular, React, IONIC, Vaadin, Apache Wicket, JSF, Swing, JavaFX
-->

<div class="hide-print">-> <a href="/projects">Weiter zu den Projekten</a></div>
