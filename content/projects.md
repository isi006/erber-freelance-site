---
title: Ausgewählte Projekte
sidebar: false 
sidebarlogo: fresh-white-alt # From (static/images/logo/)
include_footer: true
type: projects
status: 1
---

Dies ist eine Auswahl von Projekten, in denen ich mitgewirkt habe. Bei Interesse gelangen Sie hier zur <a href="/projects_full">vollständigen Projektliste</a>.
